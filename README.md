# Why Bandeirantes?

Bandeirantes é a denominação dada aos sertanistas do período colonial, que, a partir do início do século XVI, penetraram no interior da América do Sul em busca de riquezas minerais, sobretudo o ouro e a prata, abundantes na América espanhola. Contribuíram, em grande parte, para a expansão territorial do Brasil além dos limites impostos pelo Tratado de Tordesilhas, ocupando o Centro Oeste e o Sul do Brasil. E foram os descobridores do ouro em Minas Gerais, Goiás e Mato Grosso.
Fonte: [wikipedia](https://pt.wikipedia.org/wiki/Bandeirantes)

## Tests

$ mix test

## Build

$ mix escript.build

## Run

$ ./bandeirantes

```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/bandeirantes](https://hexdocs.pm/bandeirantes).

